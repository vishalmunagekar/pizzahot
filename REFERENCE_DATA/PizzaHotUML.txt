@startuml
@startuml

actor vendor #blue
actor Customer #red

vendor -- (LogIn)
vendor -- (Set Pizzas)
vendor -- (Order Processing)
vendor -- (Customer Details)
vendor -- (View Feedback)

(New Registration) -- Customer
(LogIn) -- Customer
(Search Pizzas) -- Customer
(Select Pizzas) -- Customer
(Create Order) -- Customer
(Make Payment) -- Customer
(Give Feedback) -- Customer

(New Registration) ..> (Customer Details)
(Set Pizzas) ..> (Search Pizzas)
(Set Pizzas) ..> (Select Pizzas)
(Order Processing) <.. (Create Order)
(Order Processing) <.. (Make Payment)
(View Feedback) <.. (Give Feedback)
(New Registration) ..> (LogIn)

@enduml
@enduml