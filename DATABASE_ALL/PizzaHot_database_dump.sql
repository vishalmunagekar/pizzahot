-- MySQL dump 10.13  Distrib 8.0.17, for Linux (x86_64)
--
-- Host: localhost    Database: PizzaHot
-- ------------------------------------------------------
-- Server version	8.0.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `OTP_table`
--

DROP TABLE IF EXISTS `OTP_table`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `OTP_table` (
  `user_id` int(11) NOT NULL,
  `OTP` int(11) DEFAULT NULL,
  `generated_on` date DEFAULT NULL,
  `valid_till` date DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `OTP` (`OTP`),
  CONSTRAINT `OTP_table_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user_table` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `OTP_table`
--

LOCK TABLES `OTP_table` WRITE;
/*!40000 ALTER TABLE `OTP_table` DISABLE KEYS */;
/*!40000 ALTER TABLE `OTP_table` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `address_table`
--

DROP TABLE IF EXISTS `address_table`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `address_table` (
  `address_id` int(11) NOT NULL AUTO_INCREMENT,
  `street` varchar(40) DEFAULT NULL,
  `flat_no` int(11) DEFAULT NULL,
  `city` varchar(15) DEFAULT NULL,
  `pin_code` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`address_id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `address_table_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user_table` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `address_table`
--

LOCK TABLES `address_table` WRITE;
/*!40000 ALTER TABLE `address_table` DISABLE KEYS */;
/*!40000 ALTER TABLE `address_table` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `feedback_table`
--

DROP TABLE IF EXISTS `feedback_table`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `feedback_table` (
  `order_id` int(11) DEFAULT NULL,
  `feedback_date_time` date DEFAULT NULL,
  `feedback_containt` varchar(255) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  KEY `order_id` (`order_id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `feedback_table_ibfk_1` FOREIGN KEY (`order_id`) REFERENCES `order_table` (`order_id`),
  CONSTRAINT `feedback_table_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `user_table` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `feedback_table`
--

LOCK TABLES `feedback_table` WRITE;
/*!40000 ALTER TABLE `feedback_table` DISABLE KEYS */;
/*!40000 ALTER TABLE `feedback_table` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_detail_table`
--

DROP TABLE IF EXISTS `order_detail_table`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `order_detail_table` (
  `order_id` int(11) DEFAULT NULL,
  `pizza_id` int(11) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  KEY `order_id` (`order_id`),
  CONSTRAINT `order_detail_table_ibfk_1` FOREIGN KEY (`order_id`) REFERENCES `order_table` (`order_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_detail_table`
--

LOCK TABLES `order_detail_table` WRITE;
/*!40000 ALTER TABLE `order_detail_table` DISABLE KEYS */;
/*!40000 ALTER TABLE `order_detail_table` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_table`
--

DROP TABLE IF EXISTS `order_table`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `order_table` (
  `order_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `address_id` int(11) DEFAULT NULL,
  `order_date_time` date DEFAULT NULL,
  `total_amt` int(11) DEFAULT NULL,
  `order_status` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`order_id`),
  KEY `user_id` (`user_id`),
  KEY `address_id` (`address_id`),
  CONSTRAINT `order_table_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user_table` (`user_id`),
  CONSTRAINT `order_table_ibfk_2` FOREIGN KEY (`address_id`) REFERENCES `address_table` (`address_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_table`
--

LOCK TABLES `order_table` WRITE;
/*!40000 ALTER TABLE `order_table` DISABLE KEYS */;
/*!40000 ALTER TABLE `order_table` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_table`
--

DROP TABLE IF EXISTS `password_table`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `password_table` (
  `user_id` int(11) NOT NULL,
  `changed_on` date DEFAULT NULL,
  `old_pass` varchar(50) DEFAULT NULL,
  `new_pass` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  CONSTRAINT `password_table_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user_table` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_table`
--

LOCK TABLES `password_table` WRITE;
/*!40000 ALTER TABLE `password_table` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_table` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pizza_table`
--

DROP TABLE IF EXISTS `pizza_table`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `pizza_table` (
  `pizza_id` int(11) NOT NULL AUTO_INCREMENT,
  `pizza_name` varchar(80) DEFAULT NULL,
  `pizza_category` varchar(10) DEFAULT NULL,
  `basic_price` int(11) DEFAULT NULL,
  `availability_status` varchar(2) DEFAULT NULL,
  PRIMARY KEY (`pizza_id`)
) ENGINE=InnoDB AUTO_INCREMENT=706 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pizza_table`
--

LOCK TABLES `pizza_table` WRITE;
/*!40000 ALTER TABLE `pizza_table` DISABLE KEYS */;
INSERT INTO `pizza_table` VALUES (701,'abc','veg',100,'A'),(705,'xyz','veg',300,'A');
/*!40000 ALTER TABLE `pizza_table` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `trig_pizza_after_insert` AFTER INSERT ON `pizza_table` FOR EACH ROW BEGIN
	DECLARE v_pizzaID INT;
	DECLARE v_pizzaAmount INT;
	SET v_pizzaID=NEW.pizza_id;
	SET v_pizzaAmount=NEW.basic_price;
	INSERT INTO price_size_table(pizza_id,pizza_size,pizza_price) VALUES(v_pizzaID, 'personal',v_pizzaAmount);
	SET v_pizzaID=v_pizzaID+1;
	SET v_pizzaAmount=v_pizzaAmount+150;
	INSERT INTO price_size_table(pizza_id,pizza_size,pizza_price) VALUES(v_pizzaID, 'Medium',v_pizzaAmount);
	SET v_pizzaID=v_pizzaID+1;
	SET v_pizzaAmount=v_pizzaAmount+150;
	INSERT INTO price_size_table(pizza_id,pizza_size,pizza_price) VALUES(v_pizzaID, 'family',v_pizzaAmount);
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `price_size_table`
--

DROP TABLE IF EXISTS `price_size_table`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `price_size_table` (
  `pizza_id` int(11) NOT NULL,
  `pizza_size` varchar(10) DEFAULT NULL,
  `pizza_price` int(11) DEFAULT NULL,
  PRIMARY KEY (`pizza_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `price_size_table`
--

LOCK TABLES `price_size_table` WRITE;
/*!40000 ALTER TABLE `price_size_table` DISABLE KEYS */;
INSERT INTO `price_size_table` VALUES (701,'personal',100),(702,'Medium',250),(703,'family',400),(705,'personal',300),(706,'Medium',450),(707,'family',600);
/*!40000 ALTER TABLE `price_size_table` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roll_table`
--

DROP TABLE IF EXISTS `roll_table`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `roll_table` (
  `roll` varchar(10) DEFAULT NULL,
  `roll_id` int(11) NOT NULL,
  PRIMARY KEY (`roll_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roll_table`
--

LOCK TABLES `roll_table` WRITE;
/*!40000 ALTER TABLE `roll_table` DISABLE KEYS */;
/*!40000 ALTER TABLE `roll_table` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_table`
--

DROP TABLE IF EXISTS `user_table`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user_table` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(50) DEFAULT NULL,
  `user_email` varchar(60) DEFAULT NULL,
  `contact_no` bigint(20) DEFAULT NULL,
  `DOB` date DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `roll_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `user_email` (`user_email`),
  UNIQUE KEY `contact_no` (`contact_no`),
  KEY `roll_id` (`roll_id`),
  CONSTRAINT `user_table_ibfk_1` FOREIGN KEY (`roll_id`) REFERENCES `roll_table` (`roll_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_table`
--

LOCK TABLES `user_table` WRITE;
/*!40000 ALTER TABLE `user_table` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_table` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-10-11 20:29:03
